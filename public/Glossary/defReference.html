<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="../css/styleGit.css" type="text/css">
	</head>
	<body>
		<div class="definitionCapsule">
			<div class="term">Reference</div>
			<div class="title">Definition:</div>
			<div class="info">
				<ol>
					<li>A pointer to a <a href="index.html#def_git_object">Git object</a>; The official Git documentation is unclear about whether a reference can point to another reference</li>
					<li>A string representing a location where the reference is found under <code>.git</code></li>
				</ol>
			</div>
			<div class="title">Related Terms:</div>
			<div class="info">
				<p>
					<ul>
						<li><a href="index.html#def_branch">Branch</a></li>
						<li><a href="index.html#def_branch_reference">Branch Reference</a></li>
						<li><a href="index.html#def_commit_history">Commit History</a></li>
						<li><a href="index.html#def_detached_HEAD">Detached HEAD</a></li>
						<li><a href="index.html#def_git_object">Git Object</a></li>
						<li><a href="index.html#def_head">head</a></li>
						<li><a href="index.html#def_HEAD">HEAD</a></li>
						<li><a href="index.html#def_index">Index</a></li>
						<li><a href="index.html#def_local_reference">Local Reference</a></li>
						<li><a href="index.html#def_ref">Ref</a></li>
						<li><a href="index.html#def_refspec">Refspec</a></li>
						<li><a href="index.html#def_remote_reference">Remote Reference</a></li>
						<li><a href="index.html#def_symbolic_reference">Symbolic Reference</a></li>
						<li><a href="index.html#def_symref">Symref</a></li>
						<li><a href="index.html#def_tag">Tag</a></li>
					</ul>
				</p>
			</div>
			<div class="title">How Other Terms Relate to the Term "Reference"</div>
			<div class="info">
				<p>References have been made very confusing by the Git documentation, so here is a list of terms and how they classify as references.</p>
				<p>
					<ul>
						<li><a href="index.html#def_branch">Branch</a> is a reference (but a reference is not necessarily a branch)</li>
						<li><a href="index.html#def_head">head</a> is a reference because it is a synonym for <a href="index.html#def_branch">branch</a></li>
						<li><a href="index.html#def_HEAD">HEAD</a> may or may not be a reference; The Git documentation is unclear</li>
						<ul><li><a href="index.html#def_detached_HEAD">detached HEAD</a> is definitively a reference because this term is defined to be HEAD when it points directly to a commit</li></ul>
						<li><a href="index.html#def_index">Index</a> is not a reference</li>
						<ul>
							<li>The official Git definition, "index" means "staging index" (or "staging area") which might use references, but it is definitely not one</li>
							<li>I redefined "index" to mean a list of entries; a singular entry could be a reference or it could be something else entirely</li>
						</ul>
						<li><a href="index.html#def_ref">Ref</a> is an abbreviation for "reference"</li>
						<li><a href="index.html#def_refspec">Refspec</a> is not a reference (the purpose of "refspec" is to define a mapping between a reference in a local repository and a reference in a remote repository)</li>
						<li><a href="index.html#def_remote_reference">Remote Reference</a> is a reference; both official definitions of remote reference meet the criteria</li>
						<li><a href="index.html#def_symbolic_reference">Symbolic Reference</a> is either a specialized kind of reference or a completely different concept than reference; the Git documentation is unclear (see additional notes below)</li>
						<li><a href="index.html#def_symref">Symref</a> is a synonym for "symbolic reference"</li>
						<li><a href="index.html#def_tag">Tag</a> is a reference under some circumstances, but not a reference in other circumstances; the Git documentation is clear but only if you work very hard to piece the information together (or read my additional notes below)</li>
					</ul>
				</p>
			</div>
			<div class="title">Sources of My Claims</div>
			<div class="info">
				<p>I make a lot of bold claims as what is a reference and what isn't, so this entry is probably both surprising and confusing. If my explanations below do not satisfy you, it's best to read the information I provide in the individual terms.</p>
			</div>
			<div class="title">The Difference Between Reference and Symbolic Reference:</div>
			<div class="info">
				<p>It's clear that a symbolic reference is a pointer that points to another reference. The exact definition of reference, however, is unclear. In <a href="https://git-scm.com/book/en/v2/Git-Internals-Git-References#ref_the_ref">Pro Git, Chapter 10.3</a>, it states "By symbolic reference, we mean that unlike a normal reference, it contains a pointer to another reference". This means that a reference <i>cannot</i> point to other references. However, in the <a href="https://git-scm.com/docs/gitglossary#Documentation/gitglossary.txt-aiddefrefaref">Git glossary for the term "ref"</a>, it states that a reference points to an object or another reference. This means that a reference <i>can</i> point to other references. Both definitions cannot be true.</p>
				<p>From a general perspective in the IT world, there is no standard definition for "reference" that I'm aware of, so "references" may or may not include "symbolic references", although most teams take a hard stance one way or another.</p>
				<p>Personally, I don't care which definition the Git team decides upon so long as it is clear where the line is drawn. The question I would pose to them is "What is the purpose of distinguishing between the term 'reference' and the term 'symbolic reference'?" Once that question is clearly answered, then the definitions should fall into place. One thing to keep in mind: if the term "reference" includes the idea of "symbolic reference", then HEAD is not just a "symbolic reference" but also a "reference" (which I don't think was the intention of the Git team).</p>
			</div>
			<div class="title">The Difference Between Reference and Branch:</div>
			<div class="info">
				<p>My glossary is clear: a <a href="index.html#def_branch">branch</a> is a type of reference. Certain parts of the Git documentation are not as clear.</p>
				<p>A casual user might read <a href="https://git-scm.com/book/en/v2/Git-Internals-Git-References">Pro Git, Chapter 10.3</a> to learn about references without reading the rest of the book. This chapter describes how to look at references in detail by looking just at a branch. This might cause some casual users to erroneously equate the terms "branch" and "reference", but this would be incorrect.</p>
				<p>To be clear: a branch is a specific kind of reference that points only to a <a href="index.html#def_commit_object">commit object</a>. "References" in general may point to any kind of <a href="index.html#def_git_object">Git object</a>.</p>
			</div>
			<div class="title">Special Note About Tags, Git Objects, and References</div>
			<div class="info">
				<p>Simplified, we can say there are two types of tags:</p>
				<ul>
					<li><a href="index.html#def_lightweight_tag">Lightweight tags</a> save tags as "references"</li>
					<li><a href="index.html#def_annotated_tag">Annotated tags</a> save tags as <a href="index.html#def_git_object">Git objects</a></li>
				</ul>
				<p>Git documentation about tags is confusing and unclear. Fortunately, I was able to (mostly) sort out the confusion. Details can be found in my detailed definition of <a href="defTag.html">tag</a>.</p>
				<p>Unfortunately, with the confusion about reference and symbolic reference (as indicated above), it's unclear how Lightweight tags are supposed to behave and how they actually behave.</p>
			</div>
			<div class="title">Storage of References</div>
			<div class="info">
				<p>Generally speaking, references (that are not symbolic references) are stored in the Git repository under the folder <code>.git/refs</code>. There are three subfolders for the three types of references:</p>
				<p>
					<ul>
				<li><a href="index.html#def_branch">Branches</a> are stored in <code>.git/refs/heads</code> (because head is a synonym for <a href="index.html#def_branch_reference">branch reference</a>)</li>
						<li><a href="index.html#def_remote_reference">Remote references (that are stored localy)</a> are stored in <code>.git/refs/remotes</code></li>
						<li><a href="index.html#def_lightweight_tag">Lightweight tags</a> are stored in <code>.git/refs/tags</code></li>
					</ul>
				</p>

				<p>The above is not always true, though. There are two instances I'm aware of:</p>
				<p>
					<ul>
						<li>In the glossary under <a href="https://git-scm.com/docs/gitglossary#Documentation/gitglossary.txt-aiddefrefaref">ref</a>, it says there are a few references that are not places under <code>refs/</code>, the most notable example is HEAD. If this information is in Pro Git, I'm unaware of it. I believe that symbolic references are not stored in <code>.git/refs</code>, but I'm far from sure.</li>
						<li>Sometimes, for performance and storage reasons, branches are packed up and no longer stored in <code>refs/</code>. I first found this information in the <a href="https://git-scm.com/docs/gitglossary#Documentation/gitglossary.txt-aiddefheadahead">glossary entry for head</a> which also led to the man page for <a href="https://git-scm.com/docs/git-pack-refs">git-pack-refs</a>). <a href="https://git-scm.com/book/en/v2/Git-Internals-Packfiles">Pro Git, Chapter 10.4</a> seems to focus on packing <a href="index.html#def_git_object">Git objects</a> up, but not references.</li>
					</ul>
				</p>

				<p></p>
				<p><b>Note</b>: Sometimes, Git documentation talks about a reference like in terms of <code>refs/</code> (like the Git glossary entry for <a href="https://git-scm.com/docs/gitglossary#Documentation/gitglossary.txt-aiddefrefaref">ref</a>). They are actually talking about the information located in the folder <code>.git/refs</code>.</p>
			</div>

			<div class="title">Refs vs References</div>
			<div class="info">
				<p>The Git glossary states that <a href="https://git-scm.com/docs/gitglossary#Documentation/gitglossary.txt-aiddefrefaref">ref</a> is only a string that represents where a reference can be found inside of <code>.git</code>. However, <a href="https://git-scm.com/book/en/v2/Git-Internals-Git-References">Pro Git, Chapter 10.3</a> explains that reference is both an actual pointer and the string representation. The chapter also states that "ref" and "reference" are synonyms:</p>
				<blockquote>In Git, these simple names are called “references” or “refs”</blockquote>
			</div>

			<div class="title">Synonym Problem</div>
			<div class="info">
				Synonyms should not be used in technical documentation. See <a href="../TooManyUnclearTerms.html">Too Many Unclear Terms</a> for a detailed explanation.
			</div>
			<div class="title">Final Thoughts</div>
			<div class="info">
				<p>Concerning official Git documentation, I've made it fairly clear in other parts of this website that there are a lot of things I'm disappointed in. Although references are one of my bigger disappointments, I didn't specifically focus on references in the other parts of my website. (I certainly go into some detail in <a href="../DemystifyingRemotes.html">Demystifying Remotes</a>, though.)</p>
				<p>That's not to say that all of the documentation is horrible. Despite the severe terminology flaws and some organizational problems, <a href="https://git-scm.com/book/en/v2">Pro Git</a> actually has a lot of good and helpful information. When used with my glossary, I believe it can be a very helpful resource.</p>
			</div>
			<div class="title"><a class="details" href="index.html#def_reference">Return to Glossary</a></div>
		</div>
	</body>
</html>
