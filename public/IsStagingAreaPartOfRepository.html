<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="css/styleGit.css" type="text/css">
	</head>
	<body>
		<div class="docTitle">Is a Staging Area Part of a Repository?</div>
		<h1>Important Note</h1>
		<p>Before you can understand this section, you should read and understand the section <a href="TooManyUnclearTerms.html">Too Many Unclear Terms</a>.</p>

		<h1>Table of Contents</h1>
		<p>
			<div><a href="#Introduction">Introduction</a></div>
			<div><a href="#RequirementsForDrawingRepository">Requirements For Drawing a Repository</a></div>
			<div><a href="#UnclearGitDiagrams">Unclear Git Diagrams</a></div>
			<div><a href="#ThreeTrees">The Three Trees</a></div>
			<div class="indent1"><a href="#UnclearGitDiagrams_OwnAnswer">Coming Up With My Own Answer</a></div>
			<div><a href="#Conclusion">Conclusion</a></div>
		</p>

		<a name="Introduction"></a>
		<h1>Introduction</h1>

		<p>My question is very simple: Is the <a href="Glossary/index.html#def_staging_area">staging area</a> or <a href="Glossary/index.html#def_staging_index">staging index</a> part of a <a href="Glossary/index.html#def_repository">repository</a> or not?</p>

		<p>Very early on when I was learning Git, I wanted to make personal notes with diagrams because I believe one of the easiest ways to understand Git is to see diagrams, but I very quickly ran into a problem. I couldn't answer the questions "What is a staging area?" and "What is a staging index?" Today, I have a much better idea what these are, but I still can't draw a diagram because I still don't know if they are part of a repository or not.</p>

		<p>My original idea was to draw a main diagram that showed all major components of Git, not just a local repository communicating with a remote repository, but also where the user files originated (i.e., the <a href="Glossary/index.html#def_working_tree">working tree</a>).</p>

		<p class="indent1"><b>Note</b>: In Git, there are <a href="Glossary/index.html#def_local_repository">local repositories</a>, <a href="Glossary/index.html#def_external_repository">external repositories</a>, and <a href="Glossary/index.html#def_remote_repository">remote repositories</a>. For the purposes of this section, you can assume external repositories and remote repositories are the same, although I draw a distinction between the two in my glossary.</p>

		<p>I wanted to this diagram as a fixed reference from which all other diagrams refer. I thought it should have been pretty straight forward.</p>

		<p>Secondary diagrams would be things like showing a file moving from one place to the next as <code>git commit</code> and <code>git push</code> were used. The secondary diagrams would neatly fold into the main diagram. It seemed easy enough at first glance, but when I wanted to talk about the working tree, I realized this probably wasn't included in any repository. (Probably.)</p>
		<p>For a long while, I couldn't find clarification if the working tree was part of a git repository or not. (Spoiler: It's not.)</p>

		<p>Then came the real challenge -- I also needed a way to diagram <a href="Glossary/index.html#def_staging">the staging process</a> in a way that fit into my main diagram. Even today, this still has me stumped.</p>

		<a name="RequirementsForDrawingRepository"></a>
		<h1>Requirements For Drawing a Repository</h1>

		<p>Very generally speaking, if you want to draw a repository "in its entirety" with any associated peripheries you'll, you'd need to plan for the following:</p>

		<ul>
			<li><a href="Glossary/index.html#def_working_tree">Working Tree</a></li>
			<li><a href="Glossary/index.html#def_staging_index">Staging Index</a> / <a href="Glossary/index.html#def_staging_area">Staging Area</a></li>
			<li><a href="Glossary/index.html#def_commit_object">Commit Objects</a> / <a href="Glossary/index.html#def_annotated_tag">Annotated Tags</a></li>
			<li><a href="Glossary/index.html#def_reference">References</a> (<a href="Glossary/index.html#def_lightweight_tag">Lightweight Tags</a>, <a href="Glossary/index.html#def_branch">Branches</a>)</li>
			<li><a href="Glossary/index.html#def_HEAD">HEAD</a></li>
			<li><a href="Glossary/index.html#def_remote_name">Remote Names</a></li>
		</ul>

		<p class="indent1"><b>Note</b>: These items are the heart and soul of Git, but some items are optional. For instance, tags do not have to be used in a repository. Additionally, there is an option for advanced users when creating a repository (<code>--bare</code>) where working trees and staging index / staging areas are not used at all. (Utilizing the bare option requires an understanding of how to setup a Git server. Setting up an actual Git server is well outside the scope of everything I'm writing in this website.)</p>

		<a name="UnclearGitDiagrams"></a>
		<h1>Unclear Git Diagrams</h1>

		<p>My <a href="Glossary/index.html">glossary</a> is a statement as to how poor the terminology in Git is. However, Git terminology is not the only thing that suffers.</p>

		<p>In my search to find out if the staging area was included in the repository or not, I ran across <a href="https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository">Pro Git, Chapter 2.2</a>. This chapter should have made things clear what was included in a repository and what wasn't. After all, the chapter title is "Recording Change to the Repository". Unfortunately, it didn't.</p>

		<p>One major reason for this confusion was Figure 8 at the top of the chapter. It misguided me more than it helped:</p>

		<ul>
			<li>At the top of the figure, it uses four labels, but what is the exact meaning of "untracked", "unmodified", "modified", and "staged"? And how do these different phases relate to one another? How did this relate to a repository?</li>
			<ul>
				<li><b>Note</b>: In <a href="Tracking.html">What is Tracking?</a>, I detail how confusing the term "tracked" is</li>
				<li><b>Note</b>: In <a href="TooManyUnclearTerms.html">Too Many Unclear Terms</a>, I detail how confusing the term "staging index" is</li>
			</ul>
			<li>What do the X-axis and Y-axis of the figure represent? I didn't know I should read this figure <i>strictly</i> from left to right. (Not only was I was trying to read it top-to-bottom, but I was distracted by the arrows going in random left/right directions.)</li>
			<li>Why is the commit arrow going "backwards"?</li>
			<li>How does the concept of staging and committing files fit into the concept of local and remote repositories?</li>
		</ul>

		<a name="ThreeTrees"></a>
		<h1>The Three Trees</h1>

		<p>I talked about the term "The Three Trees" in <a href="TooManyUnclearTerms.html#WhyIsStagingIndexNeeded_ThreeTrees">Too Many Unclear Terms</a> and I have a detailed description in <a href="Glossary/defThreeTrees.html">my glossary</a> of what the term is, why it exists, and some problems associated with it.</p>

		<p>Defined very late via <a href="https://git-scm.com/book/en/v2/Git-Tools-Reset-Demystified#_the_three_trees">Chapter 7.7</a> but introduced in <a href="https://git-scm.com/book/en/v2/Getting-Started-What-is-Git%3F#_the_three_states">Pro Git, Chapter 1.3 as "The Three States"</a>, Pro Git talks about "The Three Trees" using various and different terms. The idea of the three trees is surprisingly popular on the internet and can be found in other unofficial documentation like <a href="https://www.atlassian.com/git/tutorials/resetting-checking-out-and-reverting">Atlassian</a>.</p>

		<p>Sadly, a "tree" is never defined until Chapter 10... and it turns out that the definition of "tree" in Chapter 10 has nothing to do with the three trees. In Chapter 10, Pro Git talks about <a href="Glossary/index.html#def_tree_object">tree objects</a> which is something completely different. This is <i>very confusing</i> to a new or casual user! Terms should be clearly defined before they explained especially since these ideas are very central to the Git.</p>

		<p>Unfortunately, Pro Git still left me with my main question: Is the staging area and staging index part of a repository?</p>

		<p>The main diagram I wanted to draw and from which all other diagrams would refer had to know where the staging area and staging index went. My main diagram would contain a local repository, a remote repository, and a working tree, but where does staging area and staging index belong? How do they fit in to the whole picture?</p>

		<p><a href="https://web.archive.org/web/20190824220531/https://git-scm.com/book/en/v1/Getting-Started-Git-Basics#The-Three-States">Version 1 of the Pro Git book</a> shows a picture of the repository being defined as the git directory while the "staging area" and "working directory" are clearly outside of the repository / git directory. This is incorrect because staging information is clearly contained within the <code>.git</code> folder.</p>

		<p class="indent1"><b>Note</b>: When I started writing this website, version 1 was still hosted on the git-scm.com website and showed up in web searches. Before I finished writing this website, version 1 seems to have been removed. I think removing version 1 is an improvement. Despite my numerous problems with version 2, it's actually much better than version 1.</p>

		<p>At the time I wrote this, Pro Git was on <a href="https://git-scm.com/book/en/v2">version 2</a> of the book, and I can't find any equivalent of this in version 2. I'm guessing the authors realized the diagram was inaccurate and pulled it, but my question was left unanswered.</p>

		<a name="UnclearGitDiagrams_OwnAnswer"></a>
		<h2>Coming Up With My Own Answer</h2>

		<p>Thinking about this logically, I know:</p>
		<ul>
			<li>The files in the working tree are modified by the user, stored in a directory at the operating system level, and are stored <i>outside</i> of the <code>.git</code> directory, so this is probably not part of the repository</li>
			<li>Commits and committed files encoded by Git are stored <i>inside</i> the <code>.git</code> directory</li>
			<li>Tags, references, and HEAD are also stored in the <code>.git</code> directory (details can be found in my glossary)</li>
			<li>Remote Names are stored in <code>.git/config</code></li>
		</ul>

		<p>I didn't know where staged files were stored, so I ran a quick test: I staged a file and discovered that it is stored in the <code>.git</code> directory. So... does that mean staged files <i>are</i> stored as part of the git repository? What about the staging index?</p>

		<p>Defining whether an item is part of a repository based on whether the information is stored in the <code>.git</code> directory, we can conclude the following:</p>

		<ul>
			<li><a href="Glossary/index.html#def_working_tree">Working Tree</a>: <b>Not Included</b></li>
			<li><a href="Glossary/index.html#def_staging_index">Staging Index</a> / <a href="Glossary/index.html#def_staging_area">Staging Area</a>: <b>Included</b></li>
			<li><a href="Glossary/index.html#def_commit_object">Commit Objects</a> / <a href="Glossary/index.html#def_annotated_tag">Annotated Tags</a>: <b>Included</b></li>
			<li><a href="Glossary/index.html#def_reference">References</a> (<a href="Glossary/index.html#def_lightweight_tag">Lightweight Tags</a>, <a href="Glossary/index.html#def_branch">Branches</a>): <b>Included</b></li>
			<li><a href="Glossary/index.html#def_HEAD">HEAD</a>: <b>Included</b></li>
			<li><a href="Glossary/index.html#def_remote_name">Remote Names</a>: <b>Included</b></li>
		</ul>

		<p>This bothers me. I never really considered staging information as part of the repository.</p>
		<p>Although I've never seen any definitive information in the Git man pages nor in Pro Git that clearly stated whether the staging area is part of the repository, <a href="https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository">Pro Git, Chapter 2.1</a> talks about "cloning a repository". Although I don't trust version 2 of Pro Git to give me a reliable definition for anything, the cloning process does not include staging information.</p>

		<p>However, this idea about cloning is interesting. Based on the idea that cloning excludes staged files, it feels like the staging index and staging area don't belong in the repository, even though it is part of the <code>.git</code> directory.</p>

		<p>At this point, I'm going to guess that that the staging area and working tree exists outside of the local repository. I hoping Murphy's law doesn't kick in. I'd hate to put a lot of time into drawing my diagrams only to redraw most of them when the next version of the git documentation comes out.</p>

		<a name="Conclusion"></a>
		<h1>Conclusion</h1>

		<p>So, what do I think is included in a repository?</p>
		<ul>
			<li><a href="Glossary/index.html#def_working_tree">Working Tree</a>: <b>Not Included</b></li>
			<li><a href="Glossary/index.html#def_staging_index">Staging Index</a> / <a href="Glossary/index.html#def_staging_area">Staging Area</a>: <b>Not Included</b></li>
			<li><a href="Glossary/index.html#def_commit_object">Commit Objects</a> / <a href="Glossary/index.html#def_annotated_tag">Annotated Tags</a>: <b>Included</b></li>
			<li><a href="Glossary/index.html#def_reference">References</a> (<a href="Glossary/index.html#def_lightweight_tag">Lightweight Tags</a>, <a href="Glossary/index.html#def_branch">Branches</a>): <b>Included</b></li>
			<li><a href="Glossary/index.html#def_HEAD">HEAD</a>: <b>Included</b></li>
			<li><a href="Glossary/index.html#def_remote_name">Remote Names</a>: <b>Included</b></li>
		</ul>


		<p>Thus, when I draw my main diagram, I should draw an additional working tree, staging area, and staging index for each repository.</p>

		<p>Hmmm... should there be a name that encompasses a working tree, a staging area, a staging index, and a repository? I won't use one in my notes, but it makes me wonder.</p>

		<p><b>Personal Note</b>: I hope it's clear I'm not trying to be pedantic. I sifted through a lot of websites with incorrect information in an attempt to figure out the truth. I have put in <i>a lot</i> of time not only to avoid stepping into the same traps that others have, but preventing you (the reader) from doing so too. Frankly, I just want my diagrams to be correctly reflect what Git is and reflect as closely as possible to official Git documentation, but the lack of clearly defined terminology in Git make me feel like that I have to write something potentially incorrect and that goes against one of my goals for this website.</p>
	</body>
</html>
